rule DHW : header_decoded
{
	meta:
		Description = "Detection for spam emails that has a dcc score of greater than or equal to 99999"
		Classification = "SPAM-M"
		DisplayMatch = "DHW"

	strings:
		$subject_1 = /(\n|^)Subject: (Re|R):\s+/ nocase
		$subject_2 = /(\n|^)Subject: FW:\s+/ nocase
		$x_helo_warning_1 = /X-HELO-Warning: Remote host.{0,100}?\(.{0,100}?messagelabs\.com\) incorrectly presented itself as .{0,100}?messagelabs\.com/ nocase
		$x_helo_warning_2 = /X-HELO-Warning: Remote host.{0,100}?\(.{0,100}?craigslist\.org\) incorrectly presented itself as .{0,100}?craigslist\.org/ nocase
		
		$x_dns_warning = /X-DNS-Warning: Reverse DNS lookup failed/ nocase
		$spf_fail = /Received-SPF: (softfail|fail)/ nocase
		
	condition:
		dcc_score >= 99999 and
		any of ($x_dns_warning,$spf_fail) and
		for all of ($subject_*) : (not $) and
		for all of ($x_helo_warning_*) : (not $)
}

rule SPFD : header_decoded
{
	meta:
		Description = "Detection for spam email flag as spam in header and the spf of the email is not validated"
		Classification = "SPAM-H"
		DisplayMatch = "SPFD"

	strings:
		$sigA_1 = /X-SPAM-FLAG: YES/ nocase
		$sigA_2 = /X-AOL-REROUTE: YES/ nocase
		
		$sigB_1 = /X-Yahoo-Forwarded: from\s(.*?)\sto/ nocase
		$sigB_2 = /X-YahooFilteredBulk: \d{1,4}\.\d{1,4}\.\d{1,4}\.\d{1,4}/ nocase
		
	condition:
		all of ( $sigA_* ) or
		all of ( $sigB_* ) and
		not spf_validated
}

rule SPH314_0: header_decoded
{
    meta:
        Description = "Detection for spam emails that has a not equal tld domains"
		Classification = "SPAM-H"
		DisplayMatch = "SPH314-0"

    strings:
        $sig_01 = /(\n|^)X-FM-White: dnswl/ nocase
		$sig_02= /Received\: from \[172\.16\.16\.20\]/ nocase

    condition:
        message_size < 8000
        and not reply_to_tld_equals_from_tld
        and not sender_domain_equals_from_domain
        and recipient_domain_equals_from_domain
        and html_body_size < 1800
		and not for any of ($sig_*) : ($)
        and not from_address_equals_reply_to_address
        and from_name_equals_reply_to_name
        and not sender_domain matches /^cp20\.com$/i
        and not sender_domain matches /^cpro20\.com$/i
        and not sender_domain matches /^cpro30\.com$/i
}

rule DHD : content_decoded
{
	meta:
		Description = "Detection for aggressive spam marketing email that has a dcc reputation greater than 60"
		Classification = "BULK"
		DisplayMatch = "DHD"

	strings:
		$sigA_1 = /List-Unsubscribe: / nocase
		$sigA_2 = /X-Campaign: / nocase
		$sigA_3 = /X-CampaignID: / nocase
		$sigA_4 = /X-Mailer: Mail Bomber/ nocase
		$sigA_5 = /X-Mailer: MaxBulk.Mailer/ nocase
		$sigA_6 = /X-Mailer: MassE-Mail/ nocase
		$sigA_7 = /X-Mailer: Mailloop/ nocase
		$sigA_8 = /X-Mailer: SmartSend/ nocase
		$sigA_9 = /X-HELO: (cp1029\.blacksun\.ca|samwambach\.com|l1\.ajoyner\.com|lists\.aila\.org)/ nocase //sender of backscatter and not good reputation
		
		$sigA1_1 = /X-HELO-Warning: Remote host 198\.252\.100\.14/ nocase
		$sigA1_2 = /List\-Unsubscribe\: <https\:\/\/fenews\.us20\.list\-manage\.com\/unsubscribe/ nocase
		
		$sigB_1 = /www\.adobe\.com/ nocase
		
		$notSigA_2 = /X-DNS-Warning: Reverse DNS lookup failed/ nocase
		$notSigA_3 = /Received-SPF: (softfail|fail)/ nocase
		$notSigA_4 = /X-HELO-Warning: / nocase
		$notSigA_5 = /(\n|^)Subject: (Re|R):\s+/ nocase
		$notSigA_6 = /(\n|^)Subject: FW:\s+/ nocase
		$notSigA_7 = /(\n|^)Subject: Automatic Reply$/ nocase
		$notSigA_8 = /(\n|^)Subject: Out of Office:\s(.{0,100})/ nocase
		$notSigA_9 = /(Subject:\s{0,100}Cro|Subject: \*\* RECOVERY: fuse|Subject: \*\* PROBLEM: fuse|Subject: \*\* PROBLEM: in|Subject: (.{0,100})\.electric\.net (.{0,100}) run output|Subject: \*\* PROBLEM: aka|Subject: \*\* RECOVERY: aka)/ nocase
		$notSigA_10 = /X-Env-From: (.{0,100})(mfg-uk|epacloud)\.com@dynect-mailer\.net/ nocase
		$notSigA_11 = /X-Env-From: administrator@jmbennet\.co\.uk/ nocase
		$notSigA_12 = /X-Env-From: reminder@nominet\.org\.uk/ nocase
		$notSigA_13 = /X-Env-From: support@msoutlookonline\.net/ nocase
		$notSigA_14 = /X-Env-From: (.{0,100})@icritical\.com/ nocase
		$notSigA_15 = /X-Env-From: mfg@kosnic\.com/ nocase
		$notSigA_16 = /(\n|^)Subject: High Priority Success Notification \(Info\) from (Symantec|Backup Exec) System Recovery on/ nocase
		$notSigA_17 = /Subject\: \[eclipse\-dev\] Initial 2020\-09/ nocase
        $notSigA_18 = /List\-Unsubscribe\:\s+<\s*(https\:\/\/(\w+\.us\d\.list\-manage\.com|kavaq\.infusionsoft\.com\/app\/optOut|mynews\.apple\.com\/subscriptions|salilab\.org\/mailman\/|mailman\.dcglug\.org\.uk\/options\/list|cu191\.infusionsoft\.com|volunteerslawsuit\.com|rede\-de\-novidades\.com)|mailto\:(\w+\-melissa\=brimar\.com|sympa\@duke\.edu\?subject\=unsubscribe|unsubscribe\@em4000\.airlinesalerts\.com|feedbackloop\+[\d\_]+\@feed\.24sender\.c))/ nocase
		$notSigA_19 = /From\:[\w\s]+<sign\@\w+\.conga\-sign\.com>/ nocase
		$notSigA_20 = /Received\: from \[51\.222\.42\.6\] by mandrillapp\.com/ nocase
        $notSigA_21 = /Received\: from (NA01\-BN1\-obe\.outbound\.protection\.outlook\.com|smtp23\.gate\.iad3b) \((52\.100\.152\.251|\[172\.31\.255\.6\])\)/ nocase
		
	condition:
		(not for any of ($notSigA_*) : ($) or any of ( $sigA1_* )) and
		(attachment_number >= 1 and
		not attachment_types matches /(pdf|doc|xls|png|jpg|p7s)/i or
		$sigA_9) and
		unique_url_numbers >= 1 and
		not $sigB_1 and
		(any of ( $sigA_* ) or
		dcc_reputation > 60)
}

rule DS300 : header_decoded
{
	meta:
		Description = "Detection for aggressive spam marketing email that has a dcc score greater than 300"
		Classification = "BULK"
		DisplayMatch = "DS300"

	strings:
		$sigA_1 = /List-Unsubscribe: / nocase
		$sigA_2 = /X-Campaign: / nocase
		$sigA_3 = /X-CampaignID: / nocase
		$sigA_4 = /X-Mailer: Mail Bomber/ nocase
		$sigA_5 = /X-Mailer: MaxBulk.Mailer/ nocase
		$sigA_6 = /X-Mailer: MassE-Mail/ nocase
		$sigA_7 = /X-Mailer: Mailloop/ nocase
		$sigA_8 = /X-Mailer: SmartSend/ nocase
		
		$sigC_1 = /Received: from CWLGBR01FT003\.eop-gbr01\.prod\.protection\.outlook\.com/ nocase
		$sigC_2 = /List\-Unsubscribe\: <https\:\/\/fenews\.us20\.list\-manage\.com\/unsubscribe/ nocase
		$sigC_3 = /List\-Unsubscribe: <https\:\/\/visitor\.constantcontact\.com\/do\?p\=un\&m\=001rmLtb5gp1Wf3FUYsVHPAtg\%3D\%3D\&se\=001EBkiBGJFxSwTXJtpA5/ nocase
		
		$sigB_1 = /www\.adobe\.com/ nocase
		
		$notSigA_1 = /Received-SPF: (softfail|fail)/ nocase
		$notSigA_2 = /X-HELO-Warning: / nocase
		$notSigA_3 = /(\n|^)Subject: (Re|R):\s+/ nocase
		$notSigA_4 = /(\n|^)Subject: FW:\s+/ nocase
		$notSigA_5 = /(\n|^)Subject: Automatic Reply/ nocase
		$notSigA_6 = /(\n|^)Subject: Out of Office:\s(.*)/ nocase
		$notSigA_7 = /(Subject:\s?Cro|Subject: \*\* RECOVERY: fuse|Subject: \*\* PROBLEM: fuse|Subject: \*\* PROBLEM: in|Subject: (.{0,100})\.electric\.net (.{0,100}) run output|Subject: \*\* PROBLEM: aka|Subject: \*\* RECOVERY: aka)/ nocase
		$notSigA_8 = /X-Env-From: administrator@jmbennet\.co\.uk/ nocase
		$notSigA_9 = /X-Env-From: (.*?)(mfg-uk|epacloud)\.com@dynect-mailer\.net/ nocase
		$notSigA_10 = /X-Env-From: reminder@nominet\.org\.uk/ nocase
		$notSigA_11 = /X-DNS-Warning: Reverse DNS lookup failed/ nocase
		$notSigA_12 = /X-Env-From: support@msoutlookonline\.net/ nocase
		$notSigA_13 = /X-Env-From: (.{0,100})@icritical\.com/ nocase
		$notSigA_14 = /X-Env-From: mfg@kosnic\.com/ nocase
		$notSigA_15 = /(\n|^)Subject: High Priority Success Notification \(Info\) from (Symantec|Backup Exec) System Recovery on/ nocase
		$notSigA_16 = /X-Env-From: sse\.notifier@gmail\.com/ nocase
        $notSigA_17 = /Subject\: Re\: \[QIAT\] hanging up a call/ nocase
        $notSigA_18 = /List\-Unsubscribe\:\s+<https\:\/\/eur0\d\.safelinks\.protection\.outlook\.com/ nocase
        $notSigA_19 = /Received\: from (\w+\.eop\-\w+0\d\.prod\.protection\.outlook\.com|smarthost0\d\.dc\d\.eharmony\.com|mta7d9\.r\.grouponmail\.nl|foundry4\.dataflame\.co\.uk|o123\.p9\.mailjet\.com|o7\.email\.masivapp\.com|69\-171\-232\-147\.mail\-mail\.facebook\.com|inxmx85\.inxserver\.de|mail\-sor\-f69\.google\.com|mail\d+\.notify\.meetmemail\.com|undelivered\d*\.vmware\.com|xpmsp45\.xprofiler\.ch|mail\-sor\-f41\.google\.com|\[<[\d\-]+\@be1\.maropost\.com|\[10\.233\.19\.21\])/ nocase
		$notSigA_20 = /List\-Unsubscribe\:\s+<mailto\:[\w\.\-\=]+\@((fbl\.en25|email1\-corel|e\.synchronyfinancial|e\.suntrust|bf01\.hubspotstarter|bf0\dx.hubspotemail)\.(com|net)\?subject\=|lists\.fedoraproject\.org|LISTSERV\.\w+\.EDU)/ nocase
		$notSigA_21 = /List\-Unsubscribe\:\s+<https?\:\/\/(my\.signpost\.com|xr\.easymegaadvice\.net|app\.icontact\.com|trailer\.web\-view\.net|ctlnk\.post\-mitteilung\.de|(\w+\.us\d+\.|cwjg\.mail)list\-manage\.com|secure\.accor\.com|www\.1200grad\.com|watchmedier\.peytzmail\.com|wvw\.yotelecom\.co\.uk|VMUGSM\.informz\.net|www\.otto\.de|jobs\.ua\/rus\/unsubscribs|instagram\.com\/accounts\/remove|www\.kwans\.date|www\.atelier\-neuchatel\.com|77\.246\.158\.206\/mail\/index\.php|\d+\.seu\d*\.cleverreach\.com|freecall24news\.ch|click\.energyswitchtwo\.co\.uk|comunicaciones\.atrevia\.com|visitor\.constantcontact\.com|www\.capperloin\.cyou|mx\.kein\.org|lists\.laquadrature\.net)/ nocase
		$notSigA_22 = /From\:[\w\s]+<sign\@\w+\.conga\-sign\.com>/ nocase
		$notSigA_23 = /List\-Unsubscribe\: https\:\/\/unsubscribe\.yandex\.ru\/unsubscribe/ nocase
		
	condition:
		(not for any of ($notSigA_*) : ($) or any of ( $sigC_* )) and
		attachment_number == 0 and
		not unique_url_numbers == 1 and
		not $sigB_1 and
		(any of ( $sigA_* ) or dcc_score > 300)
}

rule BDA : header_decoded
{
	meta:
		Description = "Detection for spam emails that has a X-Env-From: related to agent@pur3.net"
		Classification = "BULK"
		DisplayMatch = "BDA"

	strings:
		$sigA_1 = /(\n|^)X-Env-From: agent@(.{0,100})\.pur3\.net/ nocase
		
		$notSigA_1 = /(\n|^)Subject: High Priority Success Notification \(Info\) from (Symantec|Backup Exec) System Recovery on\s+/ nocase
		$notSigA_2 = /From\:[\w\s]+<sign\@\w+\.conga\-sign\.com>/ nocase

	condition:
		not for any of ($notSigA_*) : ($)
        and ($sigA_1 or
        sender_domain matches /^email\.pinterest\.com$/i or
        sender_domain matches /^pinterest\.com$/i)
}

rule TINY : header_decoded
{
	meta:
		Description = "Detection for small size spam emails"
		Classification = "SPAM-M"
		DisplayMatch = "TINY"

	strings:
		$sigA_1 = /(To|Bcc|Cc):\s?(.{0,100})@(trebnet\.com|torontomls\.net|mail\.trebnet\.com|scottishkennelclub.org|imanirealtors.com|paceunderwriters.com|ymail.com|affibody.se)/ nocase
		
		$sigB_1 = /(X-MDRemoteIP|X-Origin-IP|X-Outbound-IP): (95\.226\.176\.32|216\.183\.32\.162|167\.114\.32\.92|88\.255\.121\.40|104\.247\.72\.236|190\.210\.182\.64|200\.73\.13\.6|74\.124\.197\.49|103\.8\.27\.205|91\.203\.109\.75|108\.179\.(236\.15|238\.111)|198\.46\.86\.76|69\.28\.85\.196|67\.222\.136\.193|168\.245\.1\.236|173\.247\.245\.116|202\.162\.246\.189|213\.159\.6\.153|199\.127\.218\.13|80\.241\.220\.106|173\.247\.243\.194|87\.118\.126\.35|152\.115\.61\.233|162\.241\.159\.134|129\.125\.60\.6|115\.113\.188\.22|192\.145\.233\.114|200\.69\.107\.47|207\.244\.67\.165|192\.249\.119\.245|12\.215\.87\.2|92\.54\.22\.215|104\.47\.1\.9|40\.92\.3\.19)/ nocase
		
		$notSigA_1 = /(Subject: \bCron\b|Subject: \*\* RECOVERY: fuse|Subject: \*\* PROBLEM: fuse|Subject: \*\* PROBLEM: in|Subject: (.{0,100})\.electric\.net (.{0,100}) run output|Subject: \*\* PROBLEM: aka|Subject: \*\* RECOVERY: aka|envelope-from <\bnagios\b|envelope-from <\bicinga\b|List\-Owner\:\s?<mailto\:[a-z\-]+\@LISTSERV\.SYR\.EDU)/ nocase
		$notSigA_2 = /X-Origin-IP: 192\.109\.254\.110/ nocase
		$notSigA_3 = /X-Env-From: (.{0,100})@epacloud\.com/ nocase
		$notSigA_4 = /X-Env-From: (.{0,100})@portalapi(.{0,100})\.electric\.net/ nocase
        $notSigA_5 = /Received:\sfrom\s\[?[\w\.\-]+\]?\s\([\w\s]*\[?(47\.73\.66\.32|89\.41\.26\.57|72\.143\.21\.30|192\.168\.31\.160|187\.189\.144\.34|172\.93\.220\.30|176\.223\.208\.246|151\.248\.117\.48|104\.146\.152\.150|80\.146\.208\.78|88\.202\.186\.64|201\.80\.1\.128|151\.46\.63\.241|46\.81\.117\.177|52\.100\.151\.247|54\.240\.14\.93|173\.224\.161\.141|205\.156\.137\.72|23\.94\.54\.204|209\.12\.79\.208|10\.3\.35\.61|209\.85\.208\.175|10\.226\.49\.90|138\.210\.143\.26|47\.56\.236\.222)/ nocase
        $notSigA_6 = /Received:\sfrom\s(\[(88\.211\.49\.82|100\.112\.4\.225|208\.46\.212\.210|192\.168\.1\.207|197\.232\.65\.104|114\.41\.5\.212)\]|\[(10\.25\.236\.245|186\.183\.249\.154)\]\s\(\[?(151\.46\.63\.241|186\-183\-249\-154))/ nocase
        $notSigA_7 = /Received:\sfrom\s(172\.20\.102\.133\:49368|\[141\.101\.234\.201)/ nocase
        $notSigA_8 = /X\-Spam(reason)?\:\s(undefined|No)/ nocase
        $notSigA_9 = /X\-(Amp|Proofpoint\-SPF|MAILCLOUD\-ANTISPAM|TM\-AS|MDAV)\-Result\:\s(LOWRISK|pass|no|clean)/ nocase
        $notSigA_10 = /X\-S(uspicious|pam)\-Flag\:\sNO/ nocase
        $notSigA_11 = /X\-((Barracuda|BESS|OutGoing)\-Spam|(Anti)?Virus|Spam)\-Status\:\s(No|Clean|SCORE\=[0-4])/ nocase
        $notSigA_12 = /X\-[\w\-]*Spam\-Score\:\s[0-4]/ nocase
        $notSigA_13 = /x\-ms\-exchange/ nocase
        $notSigA_14 = /X\-YourOrg\-MailScanner\: Found to be clean/ nocase
        $notSigA_15 = /x\-microsoft\-antispam(\-untrusted)?\:\sBCL\:0/ nocase
        $notSigA_16 = /x\-originating\-ip\:\s\[(10\.16\.100\.110|3\.22\.74\.224|65\.254\.253\.83|192\.134\.52\.187|194\.151\.80\.33|152\.3\.100\.107)\]/ nocase
        $notSigA_17 = /(X\-Sender\:\swioleta\.fidryk\@smb\.pl|X\-XM\-TransactionId\:\s63B2A9A1\-C47B\-4A7A\-B07B\-9157D92206E0\-2481\-IF)/ nocase
        $notSigA_18 = /Return\-Path\:\s<(acslawsupervisor\@googlemail\.com|fiedlersnoo\@yahoo\.de)>/ nocase
        $notSigA_19 = /Message\-ID\:\s+<(CA\+zDNuSxuBa6fu\=u\+v2PGHo\_3hNw8SyxsKCwKa6oLDuebamU6Q\@mail\.gmail\.com|502721600340109\@mail\.yandex\.ru|MWHPR14MB1792F03F0032D0C8A91D98BD9B320\@MWHPR14MB1792\.namprd14\.prod\.outlook\.com|012c01ca199f\$ff586cc0\$fe094640\$\@com|AD3A83D2\-26D6\-47CF\-A22C\-8961A7EDC588\@yahoo\.com)>/ nocase
        $notSigA_20 = /Message\-ID\:\s+<[\w\$\.\-]+\@(se1\-egw\-p01d\.sys\.schwarz|(medkitdoc|spica\.telekom|www\.kuk\-ferienwohnungen)\.de|bvvexc10|smtp\-out2\.etp\-ets\.ru|(inpre3\.hes\.trendmicro|americanas|Usncpdsmtpgw1v\.poloralphlauren)\.com|jaimetorres\.net|7C2im42A\.gatorade\.com|corporateintranet\.net|[\w\.]+.capgemini.com|dian\.gov\.co|na2\.netsuite\.com|vandenboogaardreclame\.nl|wire\.reclaimhosting\.com|deliniweb01|gscaltex\.com|hdfcbank\.com|edgewave\.com|orange\.fr|g39\.mailwallremote\.com|BELLAVISTAHOMES\.COM|SAIA\.COM|msvc\-mesg\-web102|mail101c50\.megamailservers\.eu|ifoutemea2\.hes\.trendmicro\.eu|kmtx0\d|hahamed\.com|4vt\.a5ft2rp|ausxipps306\.us\.dell\.com|server2\.dnsrou\.com|AMKOR\.COM|mtkmbs05n1\.mediatek\.inc|sumushoreca\.es|www\.kuk\-ferienwohnungen\.de|expresso\.pr\.gov\.br|[\w\-]+\.sys\.schwarz|tamco\-me\.ae)>/ nocase

	condition:
		any of ( $sigA_* ) and
		for all of ($notSigA_*) : (not $) and
		sender_domain matches /^(email\.)?pinterest\.com$/i and
		url_numbers >= 1 and
		message_size < 5000 and
		(text_body_size <= 410 and html_body_size == 0 and (not for any of ($notSigA_*) : ($) or $sigB_1)) or
		(text_body_size == 0 and html_body_size <= 410 and (not for any of ($notSigA_*) : ($) or $sigB_1)) or
		(text_body_size <= 410 and html_body_size <= 410 and (not for any of ($notSigA_*) : ($) or $sigB_1)) or
		(text_body_size <= 1500 and html_body_size == 0 and (attachment_number == 1 or attachment_number == 2) and message_size > 15000 and (not for any of ($notSigA_*) : ($) or $sigB_1)) or
		(text_body_size == 0 and html_body_size <= 15000 and (attachment_number == 1 or attachment_number == 2) and message_size > 9000 and (not for any of ($notSigA_*) : ($) or $sigB_1)) or
		(text_body_size <= 1800 and html_body_size <= 18000 and attachment_number == 1 and message_size > 15000 and not attachment_types matches /(pdf|doc|xls|png)/i and not dkim_signing_domain_equals_sender_domain) or 
		(text_body_size <= 2500 and html_body_size <= 80000 and attachment_number == 1 and attachment_types matches /(ace|exe)/i)
}

rule DHRW : header_decoded
{
	meta:
		Description = "Detection spam emails that has a failed received spf and a dcc_reputation greater than 80"
		Classification = "SPAM-M"
		DisplayMatch = "DHRW"

	strings:
		$sigA_1 = /X-DNS-Warning: Reverse DNS lookup failed/ nocase
		$sigA_2 = /Received-SPF: (softfail|fail)/ nocase
		
		$notSigA_1 = /(\n|^)Subject: (Re|R):\s+/ nocase
		$notSigA_2 = /(\n|^)Subject: FW:\s+/ nocase
		$notSigA_3 = /(\n|^)X-FM-White: dnswl/ nocase
		$notSigA_4 = /(\n|^)Authentication-Results:\s+nmspam\d\.e\.nsc\.no;\s+spf=pass\s+\(nsc.no:/ nocase

	condition:
		any of ( $sigA_* ) and
		for all of ($notSigA_*) : (not $) and
		dcc_reputation >= 80
}

rule TINY_PRF_BODY : body_decoded
{
	meta:
		Description = "Detection that has a body decoded scan tag and for small size adult spam related"
		Classification = "SPAM-H"
		DisplayMatch = "TINY_PRF_BODY"

	strings:
		$sig_1 = /.*(\ba|\@)n[a\@]l(ly)?\b.*/ nocase
		$sig_2 = /.*(\ba|\@)ss+h[o0]les\b.*/ nocase
		$sig_3 = /.*(\bc|\()um+(s|ing)|\(um+\b|\bcums?s+((in|out|on)(side)?|sw[a\@]llow(ing)?s?)\b.*/ nocase
		$sig_4 = /.*(\bc|\()unts?\b.*/ nocase
		$sig_5 = /.*\$(ex+|x{2,4}|\*x+|e?x+(y|ie[s\$]t))\b.*/ nocase
		$sig_6 = /.*\b((asian|japanese|ebony|latina?)s+(bl[o0]nde?|brunet+e|red[s-]*he[a\@]d(ed)?|girl|chick|b[a\@]be|teen|c[o0]uple|beaut(y|ie))s?)\b.*/ nocase
		$sig_7 = /.*\b((bl[o0]nde?|brunet+e|red[s-]*he[a\@]d(ed?)s?)[s-]*(girl|chick|b[a\@]be|teen|c[o0]uple)s?)\b.*/ nocase
		$sig_8 = /.*\b((h[o0]t(t(est|ie|y))?|(sex+|dirt|raunch|kink)(y|ie(r|[s\$]t))|amat[eu]+r)s+(bl[o0]nde?|brunet+e|red[s-]*he[a\@]d(ed)?|girl|chick|b[a\@]be|teen|c[o0]uple|beaut(y|ie))s?)\b.*/ nocase
		$sig_9 = /.*\b(deepthr[o0][a\@]t|thr(0a|o\@|0\@)t)(ing|ed)?s?\b.*/ nocase
		$sig_10 = /.*\b(fi[s\$]+t(ed|ings?)|fi\$[s\$]+t|fi[s\$]+\$t)\b.*/ nocase
		$sig_11 = /.*\b(h[o0]tt(y|ie)s?|h0t(ter|test)?)\b.*/ nocase
		$sig_12 = /.*\b(n[a\@][s\$]|pret)tys*girl\b.*/ nocase
		$sig_13 = /.*\b(piss(ing)?|peeing)\b.*/ nocase
		$sig_14 = /.*\b(three|four)s[o0]mes?\b.*/ nocase
		$sig_15 = /.*\b(tr[a\@]nn(ie|y)|she-?male)s?\b.*/ nocase
		$sig_16 = /.*\b(your)?c[o0]cks?\b.*/ nocase
		$sig_17 = /.*\b0rgasm(s|ic)?\b.*/ nocase
		$sig_18 = /.*\b[o0]rg(y|ies?)\b.*/ nocase
		$sig_19 = /.*\b[s\$]extouri[s\$]t[s\$]?\b.*/ nocase
		$sig_20 = /.*\b[s\$]tr[a\@]p[s-]*[o0]ns?\b.*/ nocase
		$sig_21 = /.*\bb[\@&]lls\b.*/ nocase
		$sig_22 = /.*\bb[o0][o0]b(ie|y)!s?\b.*/ nocase
		$sig_23 = /.*\bb[o0]nd[a\@]ge\b.*/ nocase
		$sig_24 = /.*\bb\@ng(e[dr]|ing)?s?\b.*/ nocase
		$sig_25 = /.*\bbi[s\$]exuals?\b.*/ nocase
		$sig_26 = /.*\bbitche?s?\b.*/ nocase
		$sig_27 = /.*\bbl[o0]w[s-]*j[o0]bs?\b.*/ nocase
		$sig_28 = /.*\bbu[s\$]t(y|ie(r|[s\$]t))\b.*/ nocase
		$sig_29 = /.*\bcre[a\@]m[s-]*pie[ds]?\b.*/ nocase
		$sig_30 = /.*\bcum[s-]*f[a\@]rt(ing)?\b.*/ nocase
		$sig_31 = /.*\bdi\(ks?\b.*/ nocase
		$sig_32 = /.*\bdild[o0]s?\b.*/ nocase
		$sig_33 = /.*\bf[a\@][s\$]tl[o0]ve\b.*/ nocase
		$sig_34 = /.*\bf[a\@]ci[a\@]l(i[s\$z]ed?)?s?\b.*/ nocase
		$sig_35 = /.*\bf[o0][o0]t[s-]*j[o0]bs?\b.*/ nocase
		$sig_36 = /.*\bf[u0-9\@#\$\\%\^\&\*!]+ck(ed|ing|er)?s?\b.*/ nocase
		$sig_37 = /.*\bg[a\@]ng[s-]*b[a\@]ng(ed|ing|er)?s?\b.*/ nocase
		$sig_38 = /.*\bg\@ys?\b.*/ nocase
		$sig_39 = /.*\bgl[o0]ry[s-]*h[o0]les?\b.*/ nocase
		$sig_40 = /.*\bh0les?\b.*/ nocase
		$sig_41 = /.*\bh[a\@]nd[s-]*j[o0]bs?\b.*/ nocase
		$sig_42 = /.*\bh[a\@]rd-?c[o0]re\b.*/ nocase
		$sig_43 = /.*\bh[o0]rn(y|ier?|ie[s\$]t)\b.*/ nocase
		$sig_44 = /.*\bh[o0]{2}kup\b.*/ nocase
		$sig_45 = /.*\bhumping\b.*/ nocase
		$sig_46 = /.*\bj[i*]z+\b.*/ nocase
		$sig_47 = /.*\bl0ver?s?\b.*/ nocase
		$sig_48 = /.*\blesbi[a\@]n\b.*/ nocase
		$sig_49 = /.*\bli\(ks?\b.*/ nocase
		$sig_50 = /.*\blu[s\$]t(y|ful+|ing)?\b.*/ nocase
		$sig_51 = /.*\bm0nster\b.*/ nocase
		$sig_52 = /.*\bm0uth\b.*/ nocase
		$sig_53 = /.*\bm[a@][s\$]turb[a\@]t(e(s|d)?|ion|ing)\b.*/ nocase
		$sig_54 = /.*\bmilf\b.*/ nocase
		$sig_55 = /.*\bnipples\b.*/ nocase
		$sig_56 = /.*\bp0und(ed|ing)?s?\b.*/ nocase
		$sig_57 = /.*\bp[o0]rn([o0]|[s-]*[s\$]t[a\@]r)?s?\b.*/ nocase
		$sig_58 = /.*\bp[u#][s\$]{2}(y|ie(s\b|\$)?).*/ nocase
		$sig_59 = /.*\bpinks*z[o0]nes?\b.*/ nocase
		$sig_60 = /.*\bs\@ck(ing|ed)?s?\b.*/ nocase
		$sig_61 = /.*\bsh[a\@]g(g?(ed|ing))?s?\b.*/ nocase
		$sig_62 = /.*\bsl[a\@]nty?[s-]*eye(d|s)?\b.*/ nocase
		$sig_63 = /.*\bsm0king\b.*/ nocase
		$sig_64 = /.*\bt0ni(ght|te)\b.*/ nocase
		$sig_65 = /.*\bt0ples+\b.*/ nocase
		$sig_66 = /.*\btit(tie)?s\b.*/ nocase
		$sig_67 = /.*\btw[a@]t(t?(ed|ing))?s?\b.*/ nocase
		$sig_68 = /.*\bup-?skirts?\b.*/ nocase
		$sig_69 = /.*\bv[a@]gin[a@][ls]?\b.*/ nocase
		$sig_70 = /.*\bw(@nna\b|ann@|@nn@).*/ nocase
		$sig_71 = /.*\bwh[o0]res?\b.*/ nocase
		$sig_72 = /.*\bwith0ut\b.*/ nocase //make text_body_size > 100 to text_body_size > 100 or text_body_size == 0
		
	condition:
		any of ( $sig_* ) and
		html_body_size < 1000 and
		text_body_size > 100 and
		url_numbers >= 1 and
		message_size < 5000
}

rule TINY_PRF : header_decoded
{
	meta:
		Description = "Detection that has a header decoded scan tag and for small size adult spam related"
		Classification = "SPAM-H"
		DisplayMatch = "TINY_PRF"

	strings:
		$sig_1 = /Subject: .*(\ba|\@)n[a\@]l(ly)?\b.*/ nocase
		$sig_2 = /Subject: .*(\ba|\@)ss+(h[o0]les?|es)?\b.*/ nocase
		$sig_3 = /Subject: .*(\bc|\()um+(s|ing)|\(um+\b|\bcums?s+((in|out|on)(side)?|sw[a\@]llow(ing)?s?)\b.*/ nocase
		$sig_4 = /Subject: .*(\bc|\()unts?\b.*/ nocase
		$sig_5 = /Subject: .*(\bs|\$)(ex+|x{2,4}|\*x+|e?x+(y|ie[s\$]t))\b.*/ nocase
		$sig_6 = /Subject: .*\b((asian|japanese|ebony|latina?)s+(bl[o0]nde?|brunet+e|red[s-]*he[a\@]d(ed)?|girl|chick|b[a\@]be|teen|c[o0]uple|beaut(y|ie))s?)\b.*/ nocase
		$sig_7 = /Subject: .*\b((bl[o0]nde?|brunet+e|red[s-]*he[a\@]d(ed?)s?)[s-]*(girl|chick|b[a\@]be|teen|c[o0]uple)s?)\b.*/ nocase
		$sig_8 = /Subject: .*\b((h[o0]t(t(est|ie|y))?|(sex+|dirt|raunch|kink)(y|ie(r|[s\$]t))|amat[eu]+r)s+(bl[o0]nde?|brunet+e|red[s-]*he[a\@]d(ed)?|girl|chick|b[a\@]be|teen|c[o0]uple|beaut(y|ie))s?)\b.*/ nocase
		$sig_9 = /Subject: .*\b(deepthr[o0][a\@]t|thr(0a|o\@|0\@)t)(ing|ed)?s?\b.*/ nocase
		$sig_10 = /Subject: .*\b(fi[s\$]+t(ed|ings?)|fi\$[s\$]+t|fi[s\$]+\$t)\b.*/ nocase
		$sig_11 = /Subject: .*\b(h[o0]tt(y|ie)s?|h0t(ter|test)?)\b.*/ nocase
		$sig_12 = /Subject: .*\b(n[a\@][s\$]|pret)tys*girl\b.*/ nocase
		$sig_13 = /Subject: .*\b(piss(ing)?|peeing)\b.*/ nocase
		$sig_14 = /Subject: .*\b(three|four)s[o0]mes?\b.*/ nocase
		$sig_15 = /Subject: .*\b(tr[a\@]nn(ie|y)|she-?male)s?\b.*/ nocase
		$sig_16 = /Subject: .*\b(your)?c[o0]cks?\b.*/ nocase
		$sig_17 = /Subject: .*\b0rgasm(s|ic)?\b.*/ nocase
		$sig_18 = /Subject: .*\b[o0]rg(y|ies?)\b.*/ nocase
		$sig_19 = /Subject: .*\b[s\$]extouri[s\$]t[s\$]?\b.*/ nocase
		$sig_20 = /Subject: .*\b[s\$]lut(ty|tie(r|[s\$]t))?s?\b.*/ nocase
		$sig_21 = /Subject: .*\b[s\$]tr[a\@]p[s-]*[o0]ns?\b.*/ nocase
		$sig_22 = /Subject: .*\bb[\@&]lls\b.*/ nocase
		$sig_23 = /Subject: .*\bb[o0][o0]b(ie|y)!s?\b.*/ nocase
		$sig_24 = /Subject: .*\bb[o0]nd[a\@]ge\b.*/ nocase
		$sig_25 = /Subject: .*\bb\@ng(e[dr]|ing)?s?\b.*/ nocase
		$sig_26 = /Subject: .*\bbi[s\$]exuals?\b.*/ nocase
		$sig_27 = /Subject: .*\bbitche?s?\b.*/ nocase
		$sig_28 = /Subject: .*\bbl[o0]w[s-]*j[o0]bs?\b.*/ nocase
		$sig_29 = /Subject: .*\bbu[s\$]t(y|ie(r|[s\$]t))\b.*/ nocase
		$sig_30 = /Subject: .*\bcre[a\@]m[s-]*pie[ds]?\b.*/ nocase
		$sig_31 = /Subject: .*\bcum[s-]*f[a\@]rt(ing)?\b.*/ nocase
		$sig_32 = /Subject: .*\bdi\(ks?\b.*/ nocase
		$sig_33 = /Subject: .*\bdild[o0]s?\b.*/ nocase
		$sig_34 = /Subject: .*\bf[a\@][s\$]tl[o0]ve\b.*/ nocase
		$sig_35 = /Subject: .*\bf[a\@]ci[a\@]l(i[s\$z]ed?)?s?\b.*/ nocase
		$sig_36 = /Subject: .*\bf[o0][o0]t[s-]*j[o0]bs?\b.*/ nocase
		$sig_37 = /Subject: .*\bf[u0-9\@#\$\\%\^\&\*!]+ck(ed|ing|er)?s?\b.*/ nocase
		$sig_38 = /Subject: .*\bg[a\@]ng[s-]*b[a\@]ng(ed|ing|er)?s?\b.*/ nocase
		$sig_39 = /Subject: .*\bg\@ys?\b.*/ nocase
		$sig_40 = /Subject: .*\bgl[o0]ry[s-]*h[o0]les?\b.*/ nocase
		$sig_41 = /Subject: .*\bh0les?\b.*/ nocase
		$sig_42 = /Subject: .*\bh[a\@]nd[s-]*j[o0]bs?\b.*/ nocase
		$sig_43 = /Subject: .*\bh[a\@]rd-?c[o0]re\b.*/ nocase
		$sig_44 = /Subject: .*\bh[o0]rn(y|ier?|ie[s\$]t)\b.*/ nocase
		$sig_45 = /Subject: .*\bh[o0]{2}kup\b.*/ nocase
		$sig_46 = /Subject: .*\bhumping\b.*/ nocase
		$sig_47 = /Subject: .*\bj[i*]z+\b.*/ nocase
		$sig_48 = /Subject: .*\bl0ver?s?\b.*/ nocase
		$sig_49 = /Subject: .*\blesbi[a\@]n\b.*/ nocase
		$sig_50 = /Subject: .*\bli\(ks?\b.*/ nocase
		$sig_51 = /Subject: .*\blu[s\$]t(y|ful+|ing)?\b.*/ nocase
		$sig_52 = /Subject: .*\bm0nster\b.*/ nocase
		$sig_53 = /Subject: .*\bm0uth\b.*/ nocase
		$sig_54 = /Subject: .*\bm[a@][s\$]turb[a\@]t(e(s|d)?|ion|ing)\b.*/ nocase
		$sig_55 = /Subject: .*\bmilf\b.*/ nocase
		$sig_56 = /Subject: .*\bn[u*][d*]es?\b.*/ nocase
		$sig_57 = /Subject: .*\bnipples\b.*/ nocase
		$sig_58 = /Subject: .*\bp0und(ed|ing)?s?\b.*/ nocase
		$sig_59 = /Subject: .*\bp[o0]rn([o0]|[s-]*[s\$]t[a\@]r)?s?\b.*/ nocase
		$sig_60 = /Subject: .*\bp[u#][s\$]{2}(y|ie(s\b|\$)?).*/ nocase
		$sig_61 = /Subject: .*\bpinks*z[o0]nes?\b.*/ nocase
		$sig_62 = /Subject: .*\bs\@ck(ing|ed)?s?\b.*/ nocase
		$sig_63 = /Subject: .*\bsh[a\@]g(g?(ed|ing))?s?\b.*/ nocase
		$sig_64 = /Subject: .*\bsl[a\@]nty?[s-]*eye(d|s)?\b.*/ nocase
		$sig_65 = /Subject: .*\bsm0king\b.*/ nocase
		$sig_66 = /Subject: .*\bt0ni(ght|te)\b.*/ nocase
		$sig_67 = /Subject: .*\bt0ples+\b.*/ nocase
		$sig_68 = /Subject: .*\btit(tie)?s\b.*/ nocase
		$sig_69 = /Subject: .*\btw[a@]t(t?(ed|ing))?s?\b.*/ nocase
		$sig_70 = /Subject: .*\bup-?skirts?\b.*/ nocase
		$sig_71 = /Subject: .*\bv[a@]gin[a@][ls]?\b.*/ nocase
		$sig_72 = /Subject: .*\bw(@nna\b|ann@|@nn@).*/ nocase
		$sig_73 = /Subject: .*\bwh[o0]res?\b.*/ nocase
		$sig_74 = /Subject: .*\bwith0ut\b.*/ nocase
		
	condition:
		any of ( $sig_* ) and
		html_body_size < 1000 and
		text_body_size < 500 and
		message_size < 5000
}

rule FCK : header_decoded
{
	meta:
		Description = "Detection for adult spam that has a fuck and sex in the content"
		Classification = "SPAM-H"
		DisplayMatch = "FCK"

	strings:
		$sig_1 = /Subject: .{5,70}\bf[u0-9@#$\%^&*!]+ck(ed|ing|er)?s?\b.{10,20}/ nocase
		$sig_2 = /Subject: .{10,50}hot sex.{5,20}/ nocase
		
		$notSig_1 = /Subject: .{10,20}ISO-8859-1.{10,20}/ nocase
		
	condition:
		any of ( $sig_* ) and
		for all of ($notSig_*) : (not $) and
		html_body_size < 1000 and
		text_body_size < 500 and
		(not sender_domain matches /facebookmail.com/i or
		not sender_domain matches /bounce.twitter.com/i)
}

rule PRK : header_decoded
{
	meta:
		Description = "Detection for spam email that has newly created domain"
		Classification = "SPAM-H"
		DisplayMatch = "PRK"

	strings:
		$sig_1 = /X-Env-From: (.{0,100})@(.{0,100})\.rocks/ nocase
		$sig_2 = /X-Origin-IP: 63\.246\.134/ nocase
		$sig_3 = /(\n|^)Subject: Re: Your(.{0,100})#\d+/ nocase
		
	condition:
		all of ( $sig_* )
}

rule SPH314_1 : header_decoded
{
	meta:
		Description = "Detection for scam email related disguising as ceo"
		Classification = "SPAM-H"
		DisplayMatch = "SPH314_1"

	strings:
		$sigA_1 = /Subject: (Chaps payment|Wire Transfer Request|Chap payment|Request|CHAPS Payment Request)/ nocase
		
		$sigB_1 = /Reply-To: \"?ceo(.{0,50})@(gmail|yahoo)\.com/ nocase
		
		$sigC_1 = /X-Env-From: \w+\d+@(gmail|yahoo)\.com/ nocase
		$sigC_2 = /X-Env-From: ceo(.{0,50})@*\.(com|org|net|ca|co\.uk)/ nocase
		
	condition:
		all of ( $sigA_* ) and
		$sigB_1 and
		any of ( $sigC_* ) and
		(sender_domain_equals_from_domain or sender_domain_equals_recipient_domain or recipient_domain_equals_from_domain) and
		(html_body_size <= 410 or text_body_size <= 410) and
		message_size < 5000
}

rule TINY_PHP_GREET : content_decoded
{
	meta:
		Description = "Detection for small size spam email that has a generic greeting and an only url content"
		Classification = "SPAM-L"
		DisplayMatch = "TINY_PHP_GREET"

	strings:
		$sig_1 = /Subject: (hi|hello) \w+/ nocase
		$sig_2 = /.*\bhttps?:\/\/[^\/]+\/([a-z0-9\-\.]+\/)*[a-z0-9]+\.php\?.*/ nocase

	condition:
		all of ( $sig_* ) and
		html_body_size <= 1000 and
		text_body_size <= 500 and
		message_size < 5000
}

rule TO_EMPTY_RDNS_FAIL : header_decoded
{
	meta:
		Description = "Detection for spam email that has an empty TO field and a failed reverse DNS lookup"
		Classification = "SPAM-M"
		DisplayMatch = "TO_EMPTY_RDNS_FAIL"

	strings:
		$sig_1 = /X-DNS-Warning: Reverse DNS lookup failed/ nocase
		$sig_2 = /(\n|^)To:\s?\n/ nocase
		
	condition:
		all of ( $sig_* )
}

rule IVCH : header_decoded
{
	meta:
		Description = "Detection for small size spam email came from china"
		Classification = "SPAM-H"
		DisplayMatch = "IVCH"

	strings:
		$sigA_1 = /(\n|^)From: .{10,50}@(qq\.com|huanqiu\.com|chinacourt\.org)/ nocase
		$sigA_2 = /X-FM-GeoIP: CN/ nocase
		$sigA_3 = /(\n|^)To:\s?\n/ nocase
		
	condition:
		all of ( $sigA_* )
		and (text_body_size < 500 or html_body_size < 500)
}

rule TINY_RE_LINK : header_decoded
{
	meta:
		Description = "Detection for small size spam email that has an xmailer Apple Mail and an empty subject"
		Classification = "SPAM-L"
		DisplayMatch = "TINY_RE_LINK"

	strings:
		$sig_1 = /X-Mailer: Apple Mail/ nocase
		$sig_2 = /Subject:(\sre\:)?\s[\r\n]+/ nocase

	condition:
		all of ( $sig_* ) and
		url_numbers >= 1 and
		html_body_size < 1000 and
		text_body_size < 500 and
		message_size < 5000
}